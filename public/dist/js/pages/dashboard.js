"use strict";

$(function () {

  //Activate the iCheck Plugin
  $('input[type="checkbox"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

  //bootstrap WYSIHTML5 - text editor
  $(".textarea").wysihtml5();

  $('.daterange').daterangepicker(
          {
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
              'Last 7 Days': [moment().subtract('days', 6), moment()],
              'Last 30 Days': [moment().subtract('days', 29), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            startDate: moment().subtract('days', 29),
            endDate: moment()
          },
  function (start, end) {
    alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });


  var table = $('#attendeesTable').dataTable({
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": true,
    "bInfo": true,
    "bAutoWidth": false
  });
  $('#attendeesTable tbody').on( 'click', 'tr', function () {
    $(this).toggleClass('active');
  } );

  $('#sendPushNotificationsBtn').click( function () {
    var deviceTokens = [];
    $('#attendeesTable tbody tr.active').each(function(index, val){
      if($(val).find('td:first').attr('data-device-token') != "") {
        deviceTokens.push($(val).find('td:first').attr('data-device-token'));
      }
    });
    $('#device_token_input').val(deviceTokens.join(','));
  });

});