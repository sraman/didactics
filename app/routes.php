<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('domain' => 'didactics.in'),function(){

	Route::get('/', [
		'as'    =>      'home',
		'uses'  =>      'HomeController@home'
	]);

	Route::post('/login',[
		'as'	=>	'login',
		'uses'	=>	'UserController@login'
	]);

	Route::post('/register',[
		'as'	=>	'register',
		'uses'	=>	'UserController@register'
	]);


	Route::post('/forgotPassword',[
		'as'	=>	'forgotPassword',
		'uses'	=>	'UserController@forgotPassword'
	]);

	Route::get('/reset/{token}',[
		'as'	=>	'reset',
		'uses'	=>	'UserController@resetPassword'
	]);

	Route::post('/reset',[
		'as'	=>	'resetPassword',
		'uses'	=>	'UserController@resetPasswordProcess'
	]);


	Route::post('/application',[
		'as'	=>	'application',
		'uses'	=>	'ApplicationController@create'
	]);

	Route::get('/logout',[
		'as'	=>	'logout',
		'uses'	=>	'UserController@logout'
	]);

	Route::get('/school',[
		'as'	=>	'viewSchools',
		'uses'	=>	'SchoolController@index'
	]);

	Route::get('/previous_applications',[
		'as'	=>	'showPreviousApplications',
		'uses'	=>	'UserController@previousApplications'
	])->before('auth');

});

Route::group(array('domain' => '{slug}.didactics.in'), function()
{

	Route::get('/',[
		'as'    =>      'viewSchool',
		'uses'  =>      'SchoolController@view'
	]);

	Route::get('/apply',[
		'as'    =>      'applyForSchool',
		'uses'  =>      'SchoolController@apply'
	])->before('auth');

	Route::post('/apply',[
		'as'	=>	'applyForSchool',
		'uses'	=>	'SchoolController@applyProcess'
	]);

});






Route::group(array('before' => 'admin'), function() {

	Route::get('/admin/dashboard',[
		'as'	=>	'adminDashboard',
		'uses'	=>	'AdminController@viewDashboard'
	]);

	Route::get('/admin/schools',[
		'as'	=>	'adminSchools',
		'uses'	=>	'AdminController@schools'
	]);

	Route::any('/admin/schools/action',[
		'as'	=>	'adminSchools',
		'uses'	=>	'AdminController@schoolsAction'
	]);

	Route::get('/admin/sections',[
		'as'	=>	'adminSections',
		'uses'	=>	'AdminController@sections'
	]);

	Route::any('/admin/sections/action',[
		'as'	=>	'adminSections',
		'uses'	=>	'AdminController@sectionsAction'
	]);

	Route::get('/admin/application_fields',[
		'as'	=>	'adminApplicationFields',
		'uses'	=>	'AdminController@applicationFields'
	]);

	Route::any('/admin/application_fields/action',[
		'as'	=>	'adminApplicationFields',
		'uses'	=>	'AdminController@applicationFieldsAction'
	]);



	Route::get('/admin/locations', [
		'as' => 'adminSchools',
		'uses' => 'AdminController@locations'
	]);

	Route::any('/admin/locations/action', [
		'as' => 'adminSchools',
		'uses' => 'AdminController@locationsAction'
	]);

	Route::get('/admin/users',[
		'as'	=>	'adminUsersList',
		'uses'	=>	'AdminController@users'
	]);

	Route::get('/admin/users/{id}/changeRole',[
		'as'	=>	'adminUsersChangeRole',
		'uses'	=>	'AdminController@changeRole'
	]);

	Route::get('/admin/users/{id}/delete',[
		'as'	=>	'adminDeleteUser',
		'uses'	=>	'AdminController@deleteUser'
	]);

	Route::get('/admin/school/{id}/download',[
		'as'	=>	'applicationsDownload',
		'uses'	=>	'AdminController@applicationDownload'
	]);

});


Route::group(array('before' => 'schooladmin'), function() {

	Route::get('/schooladmin/dashboard', [
		'as' => 'schoolAdminDashboard',
		'uses' => 'SchoolAdminController@viewDashboard'
	]);

	Route::get('/schooladmin/schools', [
		'as' => 'schoolAdminSchools',
		'uses' => 'SchoolAdminController@viewSchools'
	]);

	Route::get('/schooladmin/school/{slug}', [
		'as' => 'viewSchoolAdminSchool',
		'uses' => 'SchoolAdminController@viewSchool'
	]);

});

