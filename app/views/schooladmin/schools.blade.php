@extends('schooladmin.layout')


@section('content')

<ul>
    @foreach($schools as $school)
        <li><a href="{{URL::Route('viewSchoolAdminSchool',['slug'=>$school->slug])}}"> {{$school->name}} </a></li>
    @endforeach
</ul>


@stop