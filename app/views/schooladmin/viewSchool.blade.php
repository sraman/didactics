@extends('schooladmin.layout')

@section('head')

@stop

@section('content')

@if(isset($applications[0]))

<table class="table applications">
    <tr>
        <th>Application Id</th>
    @foreach($applications[0]->values as $head)
        <th>{{ucfirst($head->option)}}</th>
    @endforeach
    </tr>
    @foreach($applications as $application)
        <tr>
            <td>{{$application->id}}</td>
            @foreach($application->values as $value)
                <td>{{$value->value}}</td>
            @endforeach
        </tr>
    @endforeach
</table>

@else

<p>No applications till now</p>

@endif

@stop

@section('script')

<script>
    $(document).ready(function(){
        $(".table").DataTable();
    });
</script>
@stop