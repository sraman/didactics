@extends('layouts/master')


@section('content')
    	<div class="container"><!-- container -->
    	    <br/>


            <div class="col-md-8 col-md-offset-2">
                <h2>Reset your password</h2>
                <form action="{{URL::route('resetPassword')}}" method="post">
                    <div class="form-group">
                        <label for="">Token</label>
                        <input class="form-control disabled" type="text" name="token" placeholder="" value="{{$token}}" />
                    </div>
                    <div class="form-group">
                        <label for="">New Password</label>
                        <input class="form-control" type="password" name="password" placeholder="Password"/>
                    </div>
                    <center>
                    <button type="submit" class="btn btn-primary">Reset password</button>
                    </center>
                </form>
            </div>
        </div><!-- container end -->
@stop

