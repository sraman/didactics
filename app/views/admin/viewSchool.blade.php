@extends('admin.layout')


@section('content')



<div style="overflow:scroll">

<a class="btn btn-success pull-right" href="{{URL::Route('applicationsDownload',['id'=>Input::get('show')])}}"> Download Application data </a>
<br/><br/><br/>

@if(isset($applications[0]))

<table class="table">
    <thead>
        <tr>
            <th>Application Id</th>
        @foreach($applications[0]->values as $head)
            <th>{{ucfirst($head->option)}}</th>
        @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($applications as $application)
            <tr>
                <td>{{$application->id}}</td>
                @foreach($application->values as $value)
                    <td>{{$value->value}}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>

@else

<p>No applications till now</p>

@endif

</div>
@stop

@section('script')

<script src="{{URL::to('/js/TableTools.js')}}"></script>
<script>
    $(document).ready(function(){
        $(".table").DataTable( {
              "dom": 'T<"clear">lfrtip',
              "tableTools": {
                  "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
              }
         });
    });
</script>
@stop