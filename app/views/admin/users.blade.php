@extends('admin.layout')


@section('content')



<div class="row">
    <div class="col-md-12">
        <div class="well">
            <table class="table">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone number</th>
                    <th>Action</th>
                </tr>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td>
                    @if($user->id!=1)
                        @if($user->role_id==0)
                           <a href="{{URL::Route('adminUsersChangeRole',['id'=>$user->id])}}?role=1" class="btn btn-success btn-sm"> Make as School Admin</a>
                           <a href="{{URL::Route('adminUsersChangeRole',['id'=>$user->id])}}?role=2" class="btn btn-success btn-sm"> Make as Admin</a>
                        @elseif($user->role_id==1)
                            <a href="{{URL::Route('adminUsersChangeRole',['id'=>$user->id])}}?role=0" class="btn btn-danger btn-sm"> Revoke School Admin status</a>
                            <a href="{{URL::Route('adminUsersChangeRole',['id'=>$user->id])}}?role=2" class="btn btn-success btn-sm"> Make as Admin</a>
                        @else
                            <a href="{{URL::Route('adminUsersChangeRole',['id'=>$user->id])}}?role=0" class="btn btn-danger btn-sm"> Revoke Admin status</a>
                            <a href="{{URL::Route('adminUsersChangeRole',['id'=>$user->id])}}?role=1" class="btn btn-success btn-sm"> Make as School Admin</a>
                        @endif
                        <a href="{{URL::Route('adminDeleteUser',['id'=>$user->id])}}" class="btn btn-danger btn-sm"> <i class="fa fa-times"></i> Delete </a>
                    @endif
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>


@stop