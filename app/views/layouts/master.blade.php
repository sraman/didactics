<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Didactics | Your Tag Line</title>

    <!-- Styles -->
    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet" type="text/css"><!-- Google web fonts -->--}}
    <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="{{asset('assets/js/dropdown-menu/dropdown-menu.css')}}" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="{{asset('assets/js/fancybox/jquery.fancybox.css')}}" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="{{asset('assets/js/audioplayer/audioplayer.css')}}" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="{{asset('assets/style.css')}}" rel="stylesheet" type="text/css"><!-- theme styles -->


    @yield('head')

  </head>

  <body role="document">

    <!-- device test, don't remove. javascript needed! -->
    <span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
    <!-- device test end -->

    <div id="k-head" class="container"><!-- container + head wrapper -->

    	<div class="row"><!-- row -->

            <nav class="k-functional-navig"><!-- functional navig -->

                {{--<ul class="list-inline pull-right">--}}
                    {{--<li><a href="#">Jobs</a></li>--}}
                    {{--<li><a href="#">Calendar</a></li>--}}
                    {{--<li><a href="#">Directions</a></li>--}}
                {{--</ul>--}}

            </nav><!-- functional navig end -->

        	<div class="col-lg-12">

        		<div id="k-site-logo" class="pull-left"><!-- site logo -->

                    <h1 class="k-logo">
                        <a href="{{URL::route('home')}}" title="Home Page">
                            <img src="{{asset('img/site-logo.png')}}" alt="Site Logo" class="img-responsive" />
                        </a>
                    </h1>

                    <a id="mobile-nav-switch" href="#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->

            	</div><!-- site logo end -->

            	<nav id="k-menu" class="k-main-navig" style="float: right;"><!-- main navig -->

                    <ul id="drop-down-left" class="k-dropdown-menu">
                        {{--<li>--}}
                            {{--<a href="news.html" title="Our School News">News</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="events.html" title="Upcoming Events">Events</a>--}}
                        {{--</li>--}}
                        <li>
                            <a href="{{URL::route('viewSchools')}}" title="Browse">Schools</a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="#" class="Pages Collection" title="More Details">Schools</a>--}}
                            {{--<ul class="sub-menu">--}}
                            {{--<li>--}}
                                    {{--<a href="#">School Profiles</a>--}}
                                    {{--<ul class="sub-menu">--}}
                                        {{--<li><a href="full-width.html">Full Width Profile</a></li>--}}
                                        {{--<li><a href="sidebar-left.html">Sidebar on Left</a></li>--}}
                                                                            {{--</ul>--}}
                                {{--</li>--}}
                                {{--<li><a href="gallery.html">Gallery</a></li>--}}
                                {{--<li><a href="404.html">404 Error</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#" title="How things work">About Us</a>--}}
                            {{--<ul class="sub-menu">--}}

                                {{--<li><a href="about-us.html">About Us</a></li>--}}
                                {{--<li><a href="our-team.html">Our Team</a></li>--}}


                            {{--</ul>--}}
                        {{--</li>--}}

                        @if(Auth::check())
                        <li>
                            <a href="#" title="Welcome">Hi, {{Auth::user()->name}}</a>
                            <ul class="sub-menu">

                                @if(Auth::user()->role_id==1)
                                     <li><a href="{{URL::route('schoolAdminDashboard')}}" target="_blank"> Goto School Admin panel </a></li>
                                @elseif(Auth::user()->role_id==2)
                                     <li><a href="{{URL::route('adminDashboard')}}" target="_blank"> Goto Admin panel </a></li>
                                @endif
                                <li><a href="{{URL::route('showPreviousApplications')}}">Previous Applications</a></li>
                                <li><a href="{{URL::route('logout')}}">Logout</a></li>
                            </ul>
                        </li>
                        @else
                        <li>
                            <a href="javascript:;" type="button" data-toggle="modal" data-target="#myModal" title="Explore more">Login/Register</a>
                        </li>
                        @endif
                    </ul>

            	</nav><!-- main navig end -->

            </div>

        </div><!-- row end -->

    </div><!-- container + head wrapper end -->

    <div id="k-body"><!-- content wrapper -->

    @if(Session::has('success'))
        <div class="alert alert-success text-center">
            {{Session::get('success')}}
        </div>
    @elseif(Session::has('error'))
        <div class="alert alert-danger text-center">
            {{Session::get('error')}}
        </div>
    @endif

        @yield('content')

</div><!-- content wrapper end -->

    <div id="k-footer"><!-- footer -->

    	<div class="container"><!-- container -->

        	<div class="row no-gutter"><!-- row -->

            	<div class="col-lg-4 col-md-4"><!-- widgets column left -->

                    <div class="col-padded col-naked">

                        <ul class="list-unstyled clear-margins"><!-- widgets -->

                        	<li class="widget-container widget_nav_menu"><!-- widgets list -->

                                <h1 class="title-widget">Useful links</h1>

                                <ul>
                                	<li><a href="#" title="menu item">Placement Exam Schedule</a></li>
                                    <li><a href="#" title="menu item">Superintendent's Hearing Audio</a></li>
                                    <li><a href="#" title="menu item">Budget Central</a></li>
                                    <li><a href="#" title="menu item">Job Opportunities - Application</a></li>
                                    <li><a href="#" title="menu item">College Acceptances as of May 12</a></li>
                                </ul>

							</li>

                        </ul>

                    </div>

                </div><!-- widgets column left end -->

                <div class="col-lg-4 col-md-4"><!-- widgets column center -->

                    <div class="col-padded col-naked">

                        <ul class="list-unstyled clear-margins"><!-- widgets -->

                        	<li class="widget-container widget_recent_news"><!-- widgets list -->

                                <h1 class="title-widget">Contact Us</h1>

                                <div itemscope itemtype="http://data-vocabulary.org/Organization">

                                	<h2 class="title-median m-contact-subject" itemprop="name">MASA Consulting Group</h2>

                                	<div class="m-contact-address" itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                                		<span class="m-contact-street" itemprop="street-address">#91/3 9th Main Road, Anna Nagar</span>
                                		<span class="m-contact-city-region"><span class="m-contact-city" itemprop="locality">Chennai</span>, <span class="m-contact-region" itemprop="region">TN</span></span>
                                		<span class="m-contact-zip-country"><span class="m-contact-zip" itemprop="postal-code">600 040</span> <span class="m-contact-country" itemprop="country-name">India</span></span>
                                	</div>

                                	<div class="m-contact-tel-fax">
                                    	<span class="m-contact-tel">Tel: <span itemprop="tel">+91 44 4354 6205</span></span>
                                    	<span class="m-contact-fax">Mob: <span itemprop="fax">+91 7299 296 299</span></span>
                                        <span class="m-contact-fax">Email: <span itemprop="fax"><a href="#">info@masaconsult.com</span></span>
                                    </div>

                                </div>

                                <div class="social-icons">

                                	<ul class="list-unstyled list-inline">

                                    	<li><a href="#" title="Contact us"><i class="fa fa-envelope"></i></a></li>
                                        <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>

                                    </ul>

                                </div>

							</li>

                        </ul>

                    </div>

                </div><!-- widgets column center end -->

                <div class="col-lg-4 col-md-4"><!-- widgets column right -->

                    <div class="col-padded col-naked">

                        <ul class="list-unstyled clear-margins"><!-- widgets -->

                        	<li class="widget-container widget_sofa_flickr"><!-- widgets list -->

                                <h1 class="title-widget">Flickr Stream</h1>

                                <ul class="k-flickr-photos list-unstyled">
                                	<li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-1.jpg')}}" alt="Photo 1" /></a></li>
                                    <li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-2.jpg')}}" alt="Photo 2" /></a></li>
                                    <li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-3.jpg')}}" alt="Photo 3" /></a></li>
                                    <li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-4.jpg')}}" alt="Photo 4" /></a></li>
                                    <li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-5.jpg')}}" alt="Photo 5" /></a></li>
                                    <li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-6.jpg')}}" alt="Photo 6" /></a></li>
                                    <li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-7.jpg')}}" alt="Photo 7" /></a></li>
                                    <li><a href="#" title="Flickr photo"><img src="{{asset('img/flickr-8.jpg')}}" alt="Photo 8" /></a></li>
                                </ul>

							</li>

                        </ul>

                    </div>

                </div><!-- widgets column right end -->

            </div><!-- row end -->

        </div><!-- container end -->

    </div><!-- footer end -->

    <div id="k-subfooter"><!-- subfooter -->

    	<div class="container"><!-- container -->

        	<div class="row"><!-- row -->

            	<div class="col-lg-12">

                	<p class="copy-text text-inverse">
                    &copy; 2015 Didactics. Designed by <a href="http://webxeto.com/" target="blank">Web Xeto</a> | All rights reserved.
                    </p>

                </div>

            </div><!-- row end -->

        </div><!-- container end -->

    </div><!-- subfooter end -->

    <div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">RESET YOUR PASSWORD</h4>
          </div>
          <div class="modal-body">
            <center>
            <form method="post" action="{{URL::Route('forgotPassword')}}">
                <input type="email" class="form-control input-lg" id="exampleInputEmail3" name="email" placeholder="Email" required="true">
                <br/>
              <button type="submit" class="btn btn-primary">Reset Password</button>
            </form>
            </center>
            <br/>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">LOGIN / REGISTER</h4>
              </div>
              <div class="modal-body">
                <center>
                <form class="form-inline" method="post" action="{{URL::Route('login')}}">
                  <div class="form-group">
                    <input type="email" class="form-control input-lg" id="exampleInputEmail3" name="email" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control input-lg" id="exampleInputPassword3" name="password" placeholder="Password">
                  </div>
                  <button type="submit" class="btn btn-primary">Login</button>
                </form>
                <a id="forgot" href="javascript:;"> Forgot Password? </a>
                </center>
                <br/>
                <center> OR </center>
                <br/>

                <form action="{{URL::route('register')}}" method="post">
                    <div class="form-group">
                        <input class="form-control" type="text" name="name" placeholder="Name"/>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="email"/>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone_number" placeholder="Phone Number"/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="password" placeholder="Password"/>
                    </div>
                    <center>
                    <button type="submit" class="btn btn-primary">Register</button>
                    </center>
                </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

    <!-- jQuery -->
    <script src="{{asset('assets/jQuery/jquery-2.1.1.min.js')}}"></script>
    <script src="{{asset('assets/jQuery/jquery-migrate-1.2.1.min.js')}}"></script>

    <!-- Bootstrap -->
    <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Drop-down -->
    <script src="{{asset('assets/js/dropdown-menu/dropdown-menu.js')}}"></script>

    <!-- Fancybox -->
	<script src="{{asset('assets/js/fancybox/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('assets/js/fancybox/jquery.fancybox-media.js')}}"></script><!-- Fancybox media -->

    <script src="{{asset('assets/js/form-wizard.js')}}"></script>
    <!-- Responsive videos -->
    <script src="{{asset('assets/js/jquery.fitvids.js')}}"></script>

    <!-- Audio player -->
	<script src="{{asset('assets/js/audioplayer/audioplayer.min.js')}}"></script>

    <!-- Pie charts -->
    <script src="{{asset('assets/js/jquery.easy-pie-chart.js')}}"></script>

    <!-- Google Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>

    <!-- Theme -->
    <script src="{{asset('assets/js/theme.js')}}"></script>


    <script>
        $(document).ready(function(){
            $('#forgot').click(function(){
                $('#myModal').modal('hide');
                $('#forgotPassword').modal('show');
            });
        })
    </script>

    @yield('script')
  </body>
</html>