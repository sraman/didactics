<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceCounter paceSocial sidebar sidebar-social footer-sticky"> <![endif]-->
<!--[if !IE]><!--><html class="paceCounter paceSocial sidebar sidebar-social footer-sticky"><!-- <![endif]-->
<head>
	<title>Didactics</title>

	<!-- Meta -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

	<!--
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
	<link rel="stylesheet/less" href="{{asset('assets/less/admin/module.admin.stylesheet-comple')}}te.less" />
	-->

		<!--[if lt IE 9]><link rel="stylesheet" href="{{asset('assets/components/library/bootstrap/css/bootstrap.min.css')}}" /><![endif]-->

		<link rel="stylesheet" href="{{asset('assets/css/admin/module.admin.stylesheet-complete.')}}min.css" />


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<script src="{{asset('assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
	<script src="{{asset('assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
	<script src="{{asset('assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
	<script src="{{asset('assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
	<script src="{{asset('assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
	<script src="{{asset('assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>	<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>

</head>
<body class=" menu-right-hidden">

	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden ">

						<!-- Main Sidebar Menu -->
		<div id="menu" class="hidden-print hidden-xs sidebar-default sidebar-brand-primary">


<div id="sidebar-social-wrapper">
	<div id="brandWrapper">
		<a href="index.html?lang=en"><span class="text">Didactics</span></a>
	</div>
	<ul class="menu list-unstyled">


						<li class="">

								<a href="#menu-44baf2c2c99e569f29b55b1cb1253a8f" data-toggle="collapse">


								<i class="icon-group"></i>

								<span>School</span>


								</a>
						</li>
	</ul>


</div>






		</div>
		<!-- // Main Sidebar Menu END -->









		<!-- Content START -->
		<div id="content">

<div class="navbar hidden-print navbar-default box main" role="navigation">


	<div class="input-group hidden-xs pull-left">
	  	<span class="input-group-addon"><i class="icon-search"></i></span>
	  	<input type="text" class="form-control" placeholder="Search a school"/>
	</div>
</div>



			<!-- <div class="layout-app">  -->
			<h3>Application form</h3>
<div class="innerLR">
	<!-- Form -->
<form class="form-horizontal margin-none" id="validateSubmitForm" method="post" autocomplete="off" action="{{URL::route('application')}}">

	<!-- Widget -->
	<div class="widget">

		<!-- Widget heading -->
		<div class="widget-head">
			<h4 class="heading">Fill the following form</h4>
		</div>
		<!-- // Widget heading END -->

		<div class="widget-body innerAll inner-2x">

			<!-- Row -->
			<div class="row innerLR">

				<!-- Column -->
				<div class="row form">
					<div class="col-md-4">

						<!-- Group -->
						<label class="control-label" for="firstname">First name</label>
						<input class="form-control" id="firstname" name="firstname" type="text" />
						<!-- // Group END -->
					</div>
					<div class="col-md-4">
						<!-- Group -->
						<label class="control-label" for="middlename">Middle name</label>
						<input class="form-control" id="middlename" name="middlename" type="text" />
						<!-- // Group END -->
					</div>
					<div class="col-md-4">
						<!-- Group -->
						<label class="control-label" for="lastname">Last name</label>
						<input class="form-control" id="lastname" name="lastname" type="text" />
						<!-- // Group END -->
					</div>
				</div>
				<div class="row form">
					<div class="col-md-4">
						<label> Date of Birth </label>
						<div class="input-group date" id="datepicker2">
						    <input class="form-control" type="text" name="dob" value="14 February 2013" />
						    <span class="input-group-addon"><i class="fa fa-th"></i></span>
						</div>
					</div>
					<div class="col-md-8">
						<!-- Group -->
						<label class="control-label" for="pob">Place of birth</label>
						<input class="form-control" id="pob" name="pob" type="text" />
						<!-- // Group END -->
					</div>
				</div>
				<div class="row form">
					<div class="col-md-12">
						<label> Address </label>
						<textarea name="address" class="form-control"></textarea>
					</div>
				</div>
				<div class="row form">
					<div class="col-md-12">
						<label> Present School </label>
						<textarea name="present_school" class="form-control"></textarea>
					</div>
				</div>
				<div class="row form">
					<div class="col-md-12">
						<label> Other Interests </label>
						<textarea name="other_interests" class="form-control"></textarea>
					</div>
				</div>
				<h4>Family Information</h4>

				<div class="row form">
					<div class="col-md-4">
						<label> Mother Name </label>
						<input class="form-control" name="mother_name"></input>
					</div>
					<div class="col-md-4">
						<label> Mother Occupation </label>
						<input class="form-control" name="mother_occupation"></input>
					</div>
					<div class="col-md-4">
						<label> Mother Qualification </label>
						<input class="form-control" name="mother_qualification"></input>
					</div>
					<div class="col-md-6">
						<label> Mother Mobile </label>
						<input class="form-control" name="mother_mobile"></input>
					</div>
					<div class="col-md-6">
						<label> Mother email </label>
						<input class="form-control" name="mother_email"></input>
					</div>
				</div>

				<div class="row form">
					<div class="col-md-4">
						<label> Father Name </label>
						<input class="form-control" name="father_name"></input>
					</div>
					<div class="col-md-4">
						<label> Father Occupation </label>
						<input class="form-control" name="father_occupation"></input>
					</div>
					<div class="col-md-4">
						<label> Father Qualification </label>
						<input class="form-control" name="father_qualification"></input>
					</div>
					<div class="col-md-6">
						<label> Father Mobile </label>
						<input class="form-control" name="father_mobile"></input>
					</div>
					<div class="col-md-6">
						<label> Father email </label>
						<input class="form-control" name="father_email"></input>
					</div>
				</div>

				<h3> UNDERSTANDING </h3>
				<p>I understand and agree that the registration of my son/ward does not guarantee admission to the school and that the registration fee is neither transferable nor refundable.</p>


				<div class="form row">
					<div class="col-md-4">
						<label> Name </label>
						<input class="form-control" name="understanding_name"></input>
					</div>
					<div class="col-md-4">
						<label> Relationship to Boy </label>
						<input class="form-control" name="relationship_to_boy"></input>
					</div>
					<div class="col-md-4">
						<label> Date </label>
						<input class="form-control" name="date"></input>
					</div>
				</div>

				<center>
					<input type="checkbox" name="tnc" value="tnc"> Agree all terms and Conditions
				</center>
			</div>
			<!-- // Row END -->

			<!-- Row -->


			<div class="separator"></div>

			<!-- Form actions -->
			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Apply now</button>
				<button type="button" class="btn btn-default"><i class="fa fa-times"></i> Cancel</button>
			</div>
			<!-- // Form actions END -->

		</div>
	</div>
	<!-- // Widget END -->

</form>
<!-- // Form END -->




</div>


		</div>
		<!-- // Content END -->



		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->

				<!-- Footer -->
		<div id="footer" class="hidden-print">

			<!--  Copyright Line -->
			<div class="copy">&copy; 2012 - 2014 - <a href="http://www.mosaicpro.biz">MosaicPro</a> - All Rights Reserved. <a href="http://themeforest.net/?ref=mosaicpro" target="_blank">Purchase Social Admin Template</a> - Current version: v2.0.0-rc8 / <a target="_blank" href="{{asset('assets/../../CHANGELOG.txt?v=v2.0.0-rc8')}}">changelog</a></div>
			<!--  End Copyright Line -->

		</div>
		<!-- // Footer END -->


	</div>
	<!-- // Main Container Fluid END -->


	<!-- Global -->
	<script data-id="App.Config">
	var App = {};	var basePath = '',
		commonPath = "{{asset('assets/')}}",
		rootPath = '../',
		DEV = false,
		componentsPath = "{{asset('assets/components/')}}";

	var primaryColor = '#25ad9f',
		dangerColor = '#b55151',
		successColor = '#609450',
		infoColor = '#4a8bc2',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';

	var themerPrimaryColor = primaryColor;

		</script>

	<script src="{{asset('assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
<script src="{{asset('assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
<script src="{{asset('assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8')}}'"></script>
<script src="{{asset('assets/plugins/forms_editors_wysihtml5/js/wysihtml5-0.3.0_rc2.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/forms_editors_wysihtml5/js/bootstrap-wysihtml5-0.0.2.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
<script src="{{asset('assets/components/forms_editors_wysihtml5/wysihtml5.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/forms_wizards/jquery.bootstrap.wizard.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
<script src="{{asset('assets/components/forms_wizards/form-wizards.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/components/menus/sidebar.main.init.js?v=v2.0.0-rc8')}}'"></script>
<script src="{{asset('assets/components/menus/sidebar.collapse.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/components/menus/menus.sidebar.chat.init.js?v=v2.0.0-rc8')}}'"></script>
<script src="{{asset('assets/plugins/other_mixitup/jquery.mixitup.min.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
<script src="{{asset('assets/plugins/other_mixitup/mixitup.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
<script src="{{asset('assets/components/core/core.init.js?v=v2.0.0-rc8')}}"></script>
<script src="{{asset('assets/plugins/forms_elements_bootstrap-datepicker/js/bootstrap-datepicker.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}'"></script>
<script src="{{asset('assets/components/forms_elements_bootstrap-datepicker/bootstrap-datepicker.init.js?v=v2.0.0-rc8&sv=v0.0.1.2')}}"></script>
</body>
</html>