@extends('layouts/master')

@section("head")

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css"/>

@stop

@section('content')

<br/>

<div class="container">

    <div class="row no-gutter"><!-- row -->

                    <div class="col-lg-8 col-md-8 col-lg-push-4 col-md-push-4"><!-- doc body wrapper -->

                        <div class="col-padded"><!-- inner custom column -->

                        	<div class="row gutter"><!-- row -->

                            	<div class="col-lg-12 col-md-12">

                            		<figure class="news-featured-image">
										<img src="{{asset('uploads/'.$school->featured_image)}}" alt="Featured image-2" class="img-responsive">
									</figure>

									 <h1 class="page-title">{{$school->name}}</h1>

                                    <div id="rootwizard" class="wizard">

                                    	<!-- Wizard heading -->
                                    	<div class="">
                                    		<ul class="nav nav-tabs">
                                    		@foreach($tabContent as $tab)
                                    			<li role="presentation"><a href="#tab{{$tab[0]->section->id}}" data-toggle="tab">{{$tab[0]->section->name}}</a></li>
                                            @endforeach
                                    		</ul>
                                    	</div>
                                    	<!-- // Wizard heading END -->

                                    	<div class="widget">

                                    		<!-- Wizard Progress bar -->
                                    		<div class="widget-head progress progress-primary" id="bar">
                                    			<div class="progress-bar">Step <strong class="step-current">1</strong> of <strong class="steps-total">3</strong> - <strong class="steps-percent">100%</strong></div>
                                    		</div>
                                    		<!-- // Wizard Progress bar END -->

                                    		<div class="widget-body">

												<form action="{{URL::route('applyForSchool',['slug'=>$school->slug])}}" method="post" id="application">

													<div class="tab-content">

														<!-- Tab content -->

														@foreach($tabContent as $tab)

														<div role="tabpanel" class="tab-pane active" id="tab{{$tab[0]->section->id}}">
															<div class="row">
															@foreach($tab as $field)
																	<br/>
																	<label for="{{$field->field_name}}">{{$field->label}}</label>
																	@if($field->field_type=='0')
																		<input type="text" name="{{$field->field_name}}" class="form-control"/>
																	@elseif($field->field_type=='1')
																		<textarea name="{{$field->field_name}}" id="" cols="30" rows="10" class="form-control"></textarea>
																	@elseif($field->field_type=='2')
																		<?php
																			$values	=	explode(',',$field->values);
																		?>
																		<select name="{{$field->field_name}}" id="" class="form-control">
																			@foreach($values as $value)
																				<option value="{{$value}}">{{$value}}</option>
																			@endforeach
																		</select>
																	@elseif($field->field_type=='3')
																		<?php
																			$values	=	explode(',',$field->values);
																		?>
																		<br/>
																			@foreach($values as $value)
																				<input type="radio" name="{{$field->field_name}}" value="{{$value}}"/> {{$value}}
																			@endforeach
																	@elseif($field->field_type=='4')
																		<input type="text" name="{{$field->field_name}}" class="form-control datepicker"/>
																	@elseif($field->field_type=='5')
																		<div class="row">
																			<div class="col-md-4">
                                                                    			<input type="text" name="{{$field->field_name}}" class="form-control datepicker dob"/>
																			</div>
																			<div class="col-md-8">
																				<div style="position: absolute;">
																				<input type="text" name="age" id="age" class="form-control" placeholder="Age" readonly style="visibility: hidden;"/>
																				</div>
																				<input type="text" name="age_calc" id="age_calc" class="form-control" readonly/>
																			</div>
                                                                    	</div>
																	@endif
															@endforeach
																			{{--<div class="separator"></div>--}}
															</div>
														</div>
														<!-- // Tab content END -->
														@endforeach
													</div>
												</form>
                                    			<!-- Wizard pagination controls -->
                                    			<ul class="pagination margin-bottom-none">
                                    				<li class="primary previous first"><a href="#" class="no-ajaxify">First</a></li>
                                    				<li class="primary previous"><a href="#" class="no-ajaxify">Previous</a></li>
                                    				<li class="last primary"><a href="#" class="no-ajaxify">Last</a></li>
                                    			  	<li class="next primary"><a href="#" class="no-ajaxify">Next</a></li>
                                    				<li class="next finish primary" style="display:none;"><a href="" class="">Finish</a></li>
                                    			</ul>
                                    			<!-- // Wizard pagination controls END -->

                                    		</div>
                                    	</div>
                                    </div>
                                </div>

                            </div><!-- row end -->


                        </div><!-- inner custom column end -->

                    </div><!-- doc body wrapper end -->

                    <div id="k-sidebar" class="col-lg-4 col-md-4 col-lg-pull-8 col-md-pull-8"><!-- sidebar wrapper -->

                        <div class="col-padded col-shaded"><!-- inner custom column -->


                        <img src="{{asset('uploads/'.$school->logo)}}"/>

                            <p>{{$school->about_us}}</p>

                        </div><!-- inner custom column end -->

                    </div><!-- sidebar wrapper end -->

                </div>

</div>
@stop

@section('script')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
$(function()
{

	var $validator = $("#application").validate({
        		  rules: {

        		    @foreach($tabContent as $tab)
                    @foreach($tab as $field)
						@if($field->is_mandatory)
							{{$field->field_name}} : {
								required: true
							},
						@endif

						@if($field->field_type == 5)
							age : {
								required: true,
								min: 3
							},
						@endif
					@endforeach
                    @endforeach

        		  }
        		});

			function calculateAge_full(birthday) { // birthday is a date
				birthday	=	new Date(birthday);
				var diff_date = Date.now() - birthday.getTime();

                var years = Math.floor(diff_date/31536000000);
                var months = Math.floor((diff_date % 31536000000)/2628000000);
                var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
                return years+" year(s) "+months+" month(s) "+days+" day(s)";
			}

			function calculateAge(birthday) { // birthday is a date
				birthday	=	new Date(birthday);
				var diff_date = Date.now() - birthday.getTime();

				var years = Math.floor(diff_date/31536000000);
				var months = Math.floor((diff_date % 31536000000)/2628000000);
				var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
				return years;
			}

			$('.dob').on('change',function(){
				$('#age').val(calculateAge($('.dob').val()));
				$('#age_calc').val(calculateAge_full($('.dob').val()));
			});

	var bWizardTabClass = '';
	$('.wizard').each(function()
	{
		if ($(this).is('#rootwizard'))
			bWizardTabClass = 'bwizard-steps';
		else
			bWizardTabClass = '';

		var wiz = $(this);

		$(this).bootstrapWizard(
		{
			onNext: function(tab, navigation, index)
			{
				var $valid = $("#application").valid();
            	  			if(!$valid) {
            	  				$validator.focusInvalid();
            	  				return false;
            	  			}
				if(index==1)
				{
					// Make sure we entered the title
//					if(!wiz.find('#inputTitle').val()) {
//						alert('You must enter the product title');
//						wiz.find('#inputTitle').focus();
//						return false;
//					}
				}
			},
			onLast: function(tab, navigation, index)
			{
				// Make sure we entered the title
//				if(!wiz.find('#inputTitle').val()) {
//					alert('You must enter the product title');
//					wiz.find('#inputTitle').focus();
//					return false;
//				}
			},
			onTabClick: function(tab, navigation, index)
			{
				// Make sure we entered the title
//				if(!wiz.find('#inputTitle').val()) {
//					alert('You must enter the product title');
//					wiz.find('#inputTitle').focus();
//					return false;
//				}
			},
			onTabShow: function(tab, navigation, index)
			{
				var $total = navigation.find('li:not(.status)').length;
				var $current = index;
				var $percent = ($current/$total) * 100;


				if (wiz.find('.progress-bar').length)
				{
					wiz.find('.progress-bar').css({width:$percent+'%'});
					wiz.find('.progress-bar')
						.find('.step-current').html($current)
						.parent().find('.steps-total').html($total)
						.parent().find('.steps-percent').html(Math.round($percent) + "%");
				}

				// update status
				if (wiz.find('.step-current').length) wiz.find('.step-current').html($current);
				if (wiz.find('.steps-total').length) wiz.find('.steps-total').html($total);
				if (wiz.find('.steps-complete').length) wiz.find('.steps-complete').html(($current-1));

				// mark all previous tabs as complete
				navigation.find('li:not(.status)').removeClass('primary');
				navigation.find('li:not(.status):lt('+($current-1)+')').addClass('primary');

				// If it's the last tab then hide the last button and show the finish instead
				if($current+1 >= $total) {
					wiz.find('.pagination .next').hide();
					wiz.find('.pagination .finish').show();
					wiz.find('.pagination .finish').removeClass('disabled');
				} else {
					wiz.find('.pagination .next').show();
					wiz.find('.pagination .finish').hide();
				}
			},
			tabClass: bWizardTabClass,
			nextSelector: '.next',
			previousSelector: '.previous',
			firstSelector: '.first',
			lastSelector: '.last'
		});

		wiz.find('.finish').click(function()
		{
			$('#application').submit();
			return false;
//			wiz.find("a[data-toggle*='tab']:first").trigger('click');
		});
	});


	$(function () {
		$('.datepicker').datepicker();
	});
});
</script>

@stop