@extends('layouts/master')


@section('content')

<br/>

<div class="container">

    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

        <ol class="breadcrumb">
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li class="active">Schools</li>
        </ol>

    </div><!-- breadcrumbs end -->

    <div class="row no-gutter"><!-- row -->

        <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->

            <div class="col-padded"><!-- inner custom column -->

                <div class="row gutter"><!-- row -->

                    <div class="col-lg-12 col-md-12">

                        <h1 class="page-title"> Schools </h1><!-- category title -->

                        <div class="category-description"><!-- category description -->
                            <p>
                            {{--Currently, It is showing all the schools which are in our database--}}
                            </p>
                        </div>

                    </div>

                </div><!-- row end -->

                <div class="row gutter k-equal-height"><!-- row -->


                @foreach($schools as $school)

                    <div class="news-mini-wrap col-lg-6 col-md-6"><!-- news mini-wrap -->

                        <figure class="news-featured-image">
                        <a href="{{URL::route('viewSchool',['slug'=>$school->slug])}}" title="Cody Rotschild enjoys...">
                            <img src="{{asset('uploads/'.$school->featured_image)}}" alt="Featured image 1" class="img-responsive">
                        </a>
                        </figure>

                        <div class="page-title-meta">
                            <h1 class="page-title"><a href="{{URL::route('viewSchool',['slug'=>$school->slug])}}" title="Cody Rotschild enjoys...">{{$school->name}}</a></h1>
                            <div class="news-meta">
                                <span class="news-meta-date"> Applications Open </span>
                                <span class="news-meta-category"><a href="?location%5B%5D={{$school->location_id}}" title="News">{{$school->location->name}}</a></span>
                                {{--<span class="news-meta-comments"><a href="#" title="3 comments">3 comments</a></span>--}}
                            </div>
                        </div>

                        <div class="news-summary">
                            <p>
                               {{$school->description}}
                            </p>
                        </div>

                    </div><!-- news mini-wrap end -->

                @endforeach

                </div><!-- row end -->


                <div class="row gutter"><!-- row -->

                    <div class="col-lg-12">

                        {{$schools->links()}}

                    </div>

                </div><!-- row end -->

            </div><!-- inner custom column end -->

        </div><!-- doc body wrapper end -->

        <div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->

            <div class="col-padded col-shaded"><!-- inner custom column -->

                <form action="" method="GET">

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widget -->

                        <h1 class="title-widget"> Area </h1>


                        {{--@foreach($locations as $location)--}}
                             {{--<input type="checkbox" value="{{$location->id}}" name="location[]"--}}

                             {{--@if(Input::has('location'))--}}

                                 {{--@if(in_array($location->id,Input::get('location')))--}}
                                     {{--checked--}}
                                 {{--@endif--}}

                             {{--@endif--}}


                             {{--/> {{$location->name}} <br/>--}}
                        {{--@endforeach--}}

                        <select name="location" id="location" class="form-control">
                        <option value=""> SELECT LOCATION </option>
                        @foreach($locations as $location)
                            <option name="location" value="{{$location->id}}"

                            @if(Input::has('location'))

                                @if(Input::get('location')==$location->id)
                                    selected
                                @endif

                            @endif

                            /> {{$location->name}} </option>
                        @endforeach
                        </select>

                    <br/><br/>

                    </li>

                </ul><!-- widgets end -->

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widget -->

                        <h1 class="title-widget"> Board </h1>


                        {{--@foreach($boards as $board)--}}
                            {{--<input type="checkbox" name="board[]" value="{{$board->id}}"--}}

                            {{--@if(Input::has('board'))--}}

                                {{--@if(in_array($board->id,Input::get('board')))--}}
                                    {{--checked--}}
                                {{--@endif--}}

                            {{--@endif--}}

                            {{--/> {{$board->name}} <br/>--}}
                        {{--@endforeach--}}

                        <select name="board" id="board" class="form-control">
                        <option value=""> SELECT BOARD </option>
                        @foreach($boards as $board)
                            <option name="board" value="{{$board->id}}"

                            @if(Input::has('board'))

                                @if(Input::get('board')==$board->id)
                                    selected
                                @endif

                            @endif

                            /> {{$board->name}} </option>
                        @endforeach
                        </select>

                    </li>

                    <br/><br/>

                </ul><!-- widgets end -->


                 <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widget -->

                        <h1 class="title-widget"> Medium </h1>

                            {{--<input type="checkbox" name="medium[]" value="english"--}}


                            {{--@if(Input::has('medium'))--}}

                                {{--@if(in_array('english',Input::get('medium')))--}}
                                    {{--checked--}}
                                {{--@endif--}}

                            {{--@endif--}}


                            {{--/> English <br/>--}}

                            <select name="medium" id="medium" class="form-control">
                                <option value=""> SELECT MEDIUM </option>
                                <option value="english"
                                @if(Input::has('medium'))
                                    @if(Input::get('medium')=='english')
                                        selected
                                    @endif
                                @endif
                                >English</option>
                                <option value="english"
                                @if(Input::has('medium'))
                                    @if(Input::get('medium')=='tamil')
                                        selected
                                    @endif
                                @endif
                                >Tamil</option>
                                 <option value="hindi"
                                 @if(Input::has('medium'))
                                     @if(Input::get('medium')=='hindi')
                                         selected
                                     @endif
                                 @endif
                                 >Hindi</option>
                            </select>



                            {{--<input type="checkbox" name="medium[]" value="tamil"--}}

                            {{--@if(Input::has('medium'))--}}

                                {{--@if(in_array('tamil',Input::get('medium')))--}}
                                    {{--checked--}}
                                {{--@endif--}}

                            {{--@endif--}}

                            {{--/> Tamil <br/>--}}
                    </li>

                    <br/><br/>

                </ul><!-- widgets end -->


                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widget -->

                        <h1 class="title-widget"> Type </h1>

                            {{--<input type="checkbox" name="type[]" value="boys"--}}

                            {{--@if(Input::has('type'))--}}

                                {{--@if(in_array('boys',Input::get('type')))--}}
                                    {{--checked--}}
                                {{--@endif--}}

                            {{--@endif--}}


                            {{--/> Boys <br/>--}}
                            {{--<input type="checkbox" name="type[]" value="girls"--}}

                            {{--@if(Input::has('type'))--}}

                                {{--@if(in_array('girls',Input::get('type')))--}}
                                    {{--checked--}}
                                {{--@endif--}}

                            {{--@endif--}}

                            {{--/> Girls <br/>--}}
                            {{--<input type="checkbox" name="type[]" value="coed"--}}

                            {{--@if(Input::has('type'))--}}

                                {{--@if(in_array('coed',Input::get('type')))--}}
                                    {{--checked--}}
                                {{--@endif--}}

                            {{--@endif--}}

                            {{--/> Co - Ed <br/>--}}

                            <select name="type" id="type" class="form-control">
                                <option value=""> SELECT TYPE </option>
                                <option value="coed"
                                @if(Input::has('type'))
                                    @if(Input::get('type')=='coed')
                                        selected
                                    @endif
                                @endif
                                >Co-Education</option>
                                <option value="boys"
                                @if(Input::has('type'))
                                    @if(Input::get('type')=='boys')
                                        selected
                                    @endif
                                @endif
                                >Boys</option>
                                <option value="girls"
                                @if(Input::has('type'))
                                    @if(Input::get('type')=='girls')
                                        selected
                                    @endif
                                @endif
                                >Girls</option>

                            </select>
                    </li>

                    <br/><br/>

                </ul><!-- widgets end -->




                    <br/><br/>

                </ul><!-- widgets end -->

                <input type="submit" class="btn btn-success" value="Search"/>

                </form>

            </div><!-- inner custom column end -->

        </div><!-- sidebar wrapper end -->

    </div>

</div>
@stop