@extends('layouts/master')


@section('content')

<br/>

<div class="container">

    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

            <ol class="breadcrumb">
                <li><a href="{{URL::to('/')}}">Home</a></li>
                <li><a href="{{URL::to('/school')}}">Schools</a></li>
                <li class="active">{{$school->name}}</li>
            </ol>

        </div><!-- breadcrumbs end -->

    <div class="row no-gutter"><!-- row -->

                    <div class="col-lg-8 col-md-8 col-lg-push-4 col-md-push-4"><!-- doc body wrapper -->

                        <div class="col-padded"><!-- inner custom column -->

                        	<div class="row gutter"><!-- row -->

                            	<div class="col-lg-12 col-md-12">

                                    <figure class="news-featured-image">
                                        <img src="{{asset('uploads/'.$school->featured_image)}}" alt="Featured image-2" class="img-responsive">
                                    </figure>

                                    <h1 class="page-title">{{$school->name}}</h1>

                                    <div class="news-body">

                                        <div role="tabpanel">

                                          <!-- Nav tabs -->
                                          <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Overview</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">History</a></li>
                                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">About Us</a></li>
                                          </ul>

                                          <!-- Tab panes -->
                                          <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home">{{$school->overview}}</div>
                                            <div role="tabpanel" class="tab-pane" id="profile">{{$school->history}}</div>
                                            <div role="tabpanel" class="tab-pane" id="messages">{{$school->about_us}}</div>
                                          </div>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- row end -->


                        </div><!-- inner custom column end -->

                    </div><!-- doc body wrapper end -->

                    <div id="k-sidebar" class="col-lg-4 col-md-4 col-lg-pull-8 col-md-pull-8"><!-- sidebar wrapper -->

                        <div class="col-padded col-shaded"><!-- inner custom column -->


                        <img src="{{asset('uploads/'.$school->logo)}}"/>

                            <ul class="list-unstyled clear-margins"><!-- widgets -->

                            <li class="widget-container widget_nav_menu"><!-- widget -->

                                	<h1 class="title-widget">
                                	    <a href="{{(Auth::check())?URL::route('applyForSchool',['slug'=>$school->slug]):'javascript:;'}}" {{(!Auth::check())?'data-toggle="modal" data-target="#myModal"':''}} class="custom-button cb-green" title="How to apply?" target="_blank">
                                            <i class="custom-button-icon fa fa-check-square-o"></i>
                                            <span class="custom-button-wrap">
                                                <span class="custom-button-title">Apply Now !</span>
                                                <span class="custom-button-tagline">Join us whenever you feel it's time!</span>
                                            </span>
                                        </a>
                                    </h1>

                                </li>

                            </ul><!-- widgets end -->

                        </div><!-- inner custom column end -->

                    </div><!-- sidebar wrapper end -->

                </div>

</div>
@stop