@extends('layouts/master')


@section('content')

<br/>

<div class="container">

    <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->

        <ol class="breadcrumb">
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li class="active">Previous Applications</li>
        </ol>

    </div><!-- breadcrumbs end -->

    <div class="row no-gutter"><!-- row -->

    <div class="col-padded">


    <div class="row gutter">
      <table class="table">
        <tr>
            <th>Application Id</th>
            <th>School Name</th>
            <th>Status</th>
        </tr>
        @foreach($applications as $application)
        <tr>
            <td>{{$application->id}}</td>
            <td><a href="{{URL::Route('viewSchool',['slug'=>$application->school->slug])}}">{{$application->school->name}}</a></td>
            <td>Applied</td>
        </tr>
        @endforeach
      </table>
  </div>
    </div>

    </div>

</div>
@stop