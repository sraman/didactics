<?php

class School extends \Eloquent {
	protected $fillable = [];

	public function location(){
		return $this->belongsTo('Location');
	}

	public function application_fields(){
		return $this->belongsToMany('ApplicationFields', 'Application_field_School', 'school_id', 'application_field_id')->with('section');
	}

	public function users(){
		return $this->belongsToMany('User', 'school_admins', 'School_id', 'User_id');
	}
}