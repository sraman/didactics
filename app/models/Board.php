<?php

class Board extends \Eloquent {
	protected $fillable = [];

	public function schools(){
		return $this->belongsToMany('School');
	}
}