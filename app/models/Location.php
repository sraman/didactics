<?php

class Location extends \Eloquent {
	protected $fillable = [];

	public function schools(){
		return $this->belongsToMany('School');
	}
}