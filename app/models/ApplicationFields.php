<?php

class ApplicationFields extends \Eloquent {
	protected $fillable = [];

	public function section(){
		return $this->belongsTo('Section');
	}
}