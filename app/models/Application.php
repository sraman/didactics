<?php

class Application extends \Eloquent {
	protected $fillable = [];

	public function values(){
		return $this->hasMany('ApplicationValue');
	}

	public function school(){
		return $this->belongsTo('School');
	}
}