<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIsMandatoryToApplicationFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('application_fields', function(Blueprint $table)
		{
			$table->boolean('is_mandatory');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('application_fields', function(Blueprint $table)
		{
			$table->dropColumn('is_mandatory');
		});
	}

}
