<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_fields', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('section_id');
			$table->string('field_name');
			$table->string('label');
			$table->string('field_type');
			$table->text('values');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('application_fields');
	}

}
