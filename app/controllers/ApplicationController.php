<?php

class ApplicationController extends \BaseController {

	public function create(){

		$app = new Application;
		$app->first_name	=	Input::get('firstname');
		$app->middle_name	=	Input::get('middlename');
		$app->last_name		=	Input::get('lastname');
		$app->dob			=	Input::get('dob');
		$app->pob			=	Input::get('pob');
		$app->address		=	Input::get('address');
		$app->present_school	=	Input::get('present_school');
		$app->other_interest	=	Input::get('other_interests');
		$app->mother_name		=	Input::get('mother_name');
		$app->mother_occupation	=	Input::get('mother_occupation');
		$app->mother_qualification	=	Input::get('mother_qualification');
		$app->mother_email			=	Input::get('mother_email');
		$app->mother_mobile		=	Input::get('mother_mobile');
		$app->father_name		=	Input::get('father_name');
		$app->father_occupation	=	Input::get('father_occupation');
		$app->father_qualification	=	Input::get('father_qualification');
		$app->father_email			=	Input::get('father_email');
		$app->father_mobile		=	Input::get('father_mobile');
		$app->understanding_name	=	Input::get('understanding_name');
		$app->relationship_to_boy	=	Input::get('relationship_to_boy');
		$app->date 					=	Input::get('date');
		$app->save();

		echo '<div class="pm-button" style="display: block; margin: 100px auto; width: 250px;"><a href="https://www.payumoney.com/paybypayumoney/#/DD8177E24DEEB7494CE8B24042F363F2"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/132.png" /></a></div>';


	}
}