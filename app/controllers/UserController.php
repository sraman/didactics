<?php

class UserController extends \BaseController {

	public function register(){


		$userCheck = User::where('email',Input::get('email'))->first();

		if(!isset($userCheck->id)) {

			$user = new User;

			$user->name = Input::get('name');
			$user->password = Hash::make(Input::get('password'));
			$user->email = Input::get('email');
			$user->phone_number	=	Input::get('phone_number');

			$user->save();

			Auth::loginUsingId($user->id);

			Mail::send('emails.welcome', array(), function($message) use($user)
			{
				$message->to($user->email, $user->name)->subject('Welcome!');
			});

			return Redirect::back()->withSuccess('Registered successfully');

		}else{

			return Redirect::back()->withError('You are a registered user. Please login..');

		}


	}


	public function login(){

		if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))){

			return Redirect::back()->withSuccess('Logged in successfully');

		}else{

			return Redirect::back()->withError('Incorrect credentials');

		}

	}

	public function logout(){

		Auth::logout();

		return Redirect::back();

	}

	public function forgotPassword(){

		$email = Input::get('email');

		$user = User::where('email',$email)->first();

		$user->reset_token	=	str_replace('.','',str_replace('%','',stripcslashes(urlencode(Hash::make($email)))));

		$user->save();

		Mail::send('emails.auth.reminder', array('token' => $user->reset_token), function($message) use($user)
		{
			$message->to($user->email, $user->name)->subject('Welcome!');
		});

		return Redirect::to('/')->with('success','Check your email for instructions');

	}

	public function resetPassword($token){

		$user = User::where('reset_token',$token)->first();


		if(isset($user->id)){
			return View::make('reset',['token'=>$token]);
		}else{
			echo "expired link";
		}

	}

	public function resetPasswordProcess(){

		$user = User::where('reset_token',Input::get('token'))->first();

		if(isset($user->id)){
			$user->password	=	Hash::make(Input::get('password'));
			$user->save();

			return Redirect::to('/')->with('success','Password resetted. You can login now');
		}else{
			echo "expired token";
		}

	}

	public function previousApplications(){

		$data['applications'] = Application::where('user_id',Auth::user()->id)->with('school')->get();

		return View::make('previousApplications',$data);

	}

}