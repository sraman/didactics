<?php

class SchoolAdminController extends \BaseController {

	public function viewDashboard(){
		return View::make('schooladmin/dashboard');
	}


	public function viewSchools(){
		$schools = SchoolAdmin::where('user_id',Auth::user()->id)->select('school_id')->get()->toArray();

		$data['schools']	=	School::whereIn('id',$schools)->get();
		return View::make('schooladmin/schools',$data);
	}

	public function viewSchool($slug){

		$school = School::where('slug',$slug)->with('application_fields')->first();

		$data['applications'] = Application::where('school_id',$school->id)->with('values')->get();

		return View::make('schooladmin/viewSchool',$data);

	}
}