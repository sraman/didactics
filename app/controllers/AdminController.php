<?php


use Illuminate\Support\Facades\URL;

class AdminController extends \BaseController {

	public function viewDashboard(){

		return View::make('admin/dashboard');

	}


	public function schools()
	{
		$grid = DataGrid::source(new School);  //same source types of DataSet

		$grid->add('name','Name', true);

		$grid->edit('schools/action', 'Edit','show|modify|delete');
		$grid->link('admin/schools/action',"Add New", "TR");

		return View::make('admin.schools', compact('grid'));
	}


	public function schoolsAction()
	{

		if (Input::has('modify')) {
			$edit = DataEdit::source(new School);

			$edit->add('name', 'Name', 'text');
			$edit->add('description', 'Description', 'textarea');
			$edit->add('slug', 'Slug', 'text');
			$edit->add('location_id', 'Location', 'select')->options(Location::lists('name', 'id'));
			$edit->add('board_id', 'Board', 'select')->options(Board::lists('name', 'id'));
			$edit->add('overview', 'Overview', 'textarea');
			$edit->add('history', 'History', 'textarea');
			$edit->add('about_us', 'About Us', 'textarea');
			$edit->add('medium', 'Medium', 'select')->options(['english', 'tamil']);
			$edit->add('type', 'Type', 'select')->options(['boys', 'girls', 'coed']);
			$edit->add('logo', 'Logo', 'image')->move('uploads/')->preview(300, 300);
			$edit->add('featured_image', 'Featured Image', 'image')->move('uploads/')->preview(1000, 300);
			$edit->add('application_fields.label','Application Fields','tags');
			$edit->add('users.email','School Admins','tags');

			return $edit->view('admin.editSchool', compact('edit'));

		} elseif (Input::has('update')) {

			$school = School::find(Input::get('update'));

			$school->name = Input::get('name');
			$school->description = Input::get('description');
			$school->slug = Input::get('slug');
			$school->location_id = Input::get('location_id');
			$school->board_id = Input::get('board_id');
			$school->overview = Input::get('overview');
			$school->history = Input::get('history');
			$school->about_us = Input::get('about_us');
			$school->medium = Input::get('medium');
			$school->type = Input::get('type');


			if (Input::hasFile('logo')) {

				$file = Input::file('logo');
				$name = time() . '-' . $file->getClientOriginalName();
				$file = $file->move('uploads/', $name);
				$school->logo = $name;

			}


			if (Input::hasFile('featured_image')) {

				$file = Input::file('featured_image');
				$name = time() . '-' . $file->getClientOriginalName();
				$file = $file->move('uploads/', $name);
				$school->featured_image = $name;

			}


			$school->save();

			Application_field_School::where('school_id',$school->id)->delete();

			$fields = explode(',',Input::get('application_fields_label'));

			foreach($fields as $field_id){
				if($field_id != NULL) {

					$newfield = new Application_field_School;
					$newfield->school_id = $school->id;
					$newfield->Application_field_id = $field_id;
					$newfield->save();


				}
			}

			SchoolAdmin::where('school_id',$school->id)->delete();

			$users = Input::get('users_email');

			$users	=	explode(',',$users);

			foreach($users as $user_id){
				if($user_id!=NULL){

					$admin	=	new SchoolAdmin;
					$admin->School_id	=	$school->id;
					$admin->User_id		=	$user_id;
					$admin->save();

				}
			}

			return \Redirect::to('/admin/schools');
		} elseif (Input::has('delete')) {

			$school = School::find(Input::get('delete'));
			$school->delete();
			return Redirect::to('/admin/schools');

		} elseif (Input::has('process')) {

			$school = new School;

			$school->name = Input::get('name');
			$school->description = Input::get('description');
			$school->slug = Input::get('slug');
			$school->location_id = Input::get('location_id');
			$school->board_id = Input::get('board_id');
			$school->overview = Input::get('overview');
			$school->history = Input::get('history');
			$school->about_us = Input::get('about_us');
			$school->medium = Input::get('medium');
			$school->type = Input::get('type');


			if (Input::hasFile('logo')) {

				$file = Input::file('logo');
				$name = time() . '-' . $file->getClientOriginalName();
				$file = $file->move('uploads/', $name);
				$school->logo = $name;

			}

			if (Input::hasFile('featured_image')) {

				$file = Input::file('featured_image');
				$name = time() . '-' . $file->getClientOriginalName();
				$file = $file->move('uploads/', $name);
				$school->featured_image = $name;

			}

			$school->save();

			$fields = Input::get('application_fields_label');
			if($fields == ""){
				$fields	=	"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,28,22,23,24,25,26";
			}

			$fields = explode(',',$fields);

			foreach($fields as $field_id){
				if($field_id != NULL) {

					$newfield = new Application_field_School;
					$newfield->school_id = $school->id;
					$newfield->Application_field_id = $field_id;
					$newfield->save();

				}
			}

			$users = Input::get('users_email');

			$users	=	explode(',',$users);

			foreach($users as $user_id){
				if($user_id!=NULL){

					$admin	=	new SchoolAdmin;
					$admin->School_id	=	$school->id;
					$admin->User_id		=	$user_id;
					$admin->save();

				}
			}




			return Redirect::to('/admin/schools');
		} elseif(Input::has('show')){

			$data['applications'] = Application::where('school_id',Input::get('show'))->with('values')->get();

			return View::make('admin/viewSchool',$data);


		} else {

			$form = DataForm::source(new School);

			$form->add('name', 'Name', 'text');
			$form->add('description', 'Description', 'textarea');
			$form->add('slug', 'Slug', 'text');
			$form->add('location_id', 'Location', 'select')->options(Location::lists('name', 'id'));
			$form->add('board_id', 'Board', 'select')->options(Board::lists('name', 'id'));
			$form->add('overview', 'Overview', 'textarea');
			$form->add('history', 'History', 'textarea');
			$form->add('about_us', 'About Us', 'textarea');
			$form->add('medium', 'Medium', 'select')->options(['english', 'tamil']);
			$form->add('type', 'Type', 'select')->options(['boys', 'girls', 'coed']);
			$form->add('logo', 'Logo', 'image');
			$form->add('featured_image', 'Featured Image', 'image');
			$form->add('application_fields.label','Application Fields','tags');
			$form->add('users.email','School Admins','tags');

			$form->submit('Save');

			return $form->view('admin.addSchool', compact('form'));

		}
	}

	public function applicationFields()
	{
		$grid = DataGrid::source(new ApplicationFields);  //same source types of DataSet

		$grid->add('label','Label', true);

		$grid->edit('application_fields/action', 'Edit','modify|delete');
		$grid->link('admin/application_fields/action',"Add New", "TR");

		return View::make('admin.schools', compact('grid'));
	}


	/**
	 * @return mixed
     */
	public function applicationFieldsAction()
		{

			if(Input::has('modify')){
				$edit = DataEdit::source(new ApplicationFields);

				$edit->add('field_name','Field Name','text');
				$edit->add('label','Label','text');
				$edit->add('is_mandatory','Is mandatory?','checkbox');
				$edit->add('field_type','Field type','select')->options(['text','textarea','select','radio','datepicker','dateofbirth']);
				$edit->add('values','Values','textarea');
				$edit->add('section_id','Section','select')->options(Section::lists('name', 'id'));

				return $edit->view('admin.editSchool', compact('edit'));

			}elseif(Input::has('update')){

				$fields = ApplicationFields::find(Input::get('update'));

				$fields->field_name = 	Input::get('field_name');
				$fields->label 		= 	Input::get('label');
				$fields->field_type	=	Input::get('field_type');
				$fields->values		=	Input::get('values');
				$fields->section_id	=	Input::get('section_id');

				if(Input::has('is_mandatory'))
					$fields->is_mandatory	=	1;
				else
					$fields->is_mandatory	=	0;

				$fields->save();

				return \Redirect::to('/admin/application_fields');
			}elseif(Input::has('delete')){

				$fields = ApplicationFields::find(Input::get('delete'));
				$fields->delete();
				return Redirect::to('/admin/application_fields');

			}elseif(Input::has('process')){

				$fields = new ApplicationFields;

				$fields->field_name = 	Input::get('field_name');
				$fields->label 		= 	Input::get('label');
				$fields->field_type	=	Input::get('field_type');
				$fields->values		=	Input::get('values');
				$fields->section_id	=	Input::get('section_id');

				if(Input::has('is_mandatory'))
					$fields->is_mandatory	=	1;
				else
					$fields->is_mandatory	=	0;


				$fields->save();

				return Redirect::to('/admin/application_fields');
			}else{

				$form = DataForm::source(new ApplicationFields);

				$form->add('field_name','Field Name','text');
				$form->add('label','Label','text');
				$form->add('is_mandatory','Is mandatory?','checkbox');
				$form->add('field_type','Field Type','select')->options(['text','textarea','select','radio','datepicker','dateofbirth']);
				$form->add('values','Values','textarea');
				$form->add('section_id','Section','select')->options(Section::lists('name', 'id'));

				$form->submit('Save');

				return $form->view('admin.addSchool',compact('form'));

			}
		}

	public function sections()
	{
		$grid = DataGrid::source(new Section);  //same source types of DataSet

		$grid->add('name','Name');
		$grid->add('sortable_id','Sortable Id',true);

		$grid->edit('sections/action', 'Edit','modify|delete');
		$grid->link('admin/sections/action',"Add New", "TR");

		return View::make('admin.schools', compact('grid'));
	}


	public function sectionsAction()
	{

		if(Input::has('modify')){
			$edit = DataEdit::source(new Section);

			$edit->add('name','Name','text');
			$edit->add('sortable_id','Sortable Id','text');

			return $edit->view('admin.editSchool', compact('edit'));

		}elseif(Input::has('update')){

			$fields = Section::find(Input::get('update'));

			$fields->name 				= 	Input::get('name');
			$fields->sortable_id 		= 	Input::get('sortable_id');

			$fields->save();

			return \Redirect::to('/admin/sections');
		}elseif(Input::has('delete')){

			$fields = Section::find(Input::get('delete'));
			$fields->delete();
			return Redirect::to('/admin/sections');

		}elseif(Input::has('process')){

			$fields = new Section;

			$fields->name 				= 	Input::get('name');
			$fields->sortable_id 		= 	Input::get('sortable_id');

			$fields->save();

			return Redirect::to('/admin/sections');
		}else{

			$form = DataForm::source(new Section);

			$form->add('name','Name','text');
			$form->add('sortable_id','Sortable Id','text');

			$form->submit('Save');

			return $form->view('admin.addSchool',compact('form'));

		}
	}

	public function users(){

		$data['users']	=	User::paginate(10);

		return View::make('admin/users',$data);
	}

	public function changeRole($id){

		$user = User::find($id);

		$user->role_id = Input::get('role');

		$user->save();

		return Redirect::back();

	}

	public function deleteUser($id){

		$user	=	User::find($id);

		$user->delete();

		return Redirect::back();

	}

	public function locations()
	{
		$grid = DataGrid::source(new Location);  //same source types of DataSet

		$grid->add('name','Name', true);

		$grid->edit('locations/action', 'Edit','modify|delete');
		$grid->link('admin/locations/action',"Add New", "TR");

		return View::make('admin.schools', compact('grid'));
	}

	public function locationsAction()
	{

		if(Input::has('modify')){
			$edit = DataEdit::source(new Location);

			$edit->add('name','Name','text');

			return $edit->view('admin.editSchool', compact('edit'));

		}elseif(Input::has('update')){

			$location = Location::find(Input::get('update'));

			$location->name = Input::get('name');


			$location->save();

			return \Redirect::to('/admin/locations');
		}elseif(Input::has('delete')){

			$location = Location::find(Input::get('delete'));
			$location->delete();
			return Redirect::to('/admin/locations');

		}elseif(Input::has('process')){

			$location = new Location;

			$location->name = Input::get('name');

			$location->save();

			return Redirect::to('/admin/locations');
		}else{

			$form = DataForm::source(new Location);

			$form->add('name','Name','text');

			$form->submit('Save');

			return $form->view('admin.addSchool',compact('form'));

		}

	}


	public function applicationDownload($id){

		$applications = Application::where('school_id',$id)->with('values')->get();


		if(isset($applications[0])) {

			$output	= "Application Id,";

			foreach ($applications[0]->values as $head) {
				$output .= ucfirst($head->option) . ",";
			}

			$output .= "\n";

			foreach ($applications as $application) {
				$output .= $application->id . ",";
				foreach ($application->values as $value) {
					$output .= $value->value . ",";
				}
				$output .= "\n";
			}

		}else{

			$output	=	"";

		}

		$headers = array(
			'Content-Type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename="Applications.csv"',
		);

		return Response::make(rtrim($output, "\n"), 200, $headers);

	}
}