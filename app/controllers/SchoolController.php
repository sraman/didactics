<?php

class SchoolController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /school
	 *
	 * @return Response
	 */
	public function index()
	{

		$school = new School;

		if(Input::has('location')){
			$school = $school->where('location_id',Input::get('location'));
		}

		if(Input::has('board')){
			$school = $school->where('board_id',Input::get('board'));
		}

		if(Input::has('medium')){
			$school = $school->where('medium',Input::get('medium'));
		}

		if(Input::has('type')){
			$school = $school->where('type',Input::get('type'));
		}

		$data['schools']	=	$school->paginate(20);

		$data['locations']	=	Location::all();

		$data['boards']		=	Board::all();

		return View::make('school.index',$data);

	}

	public function view($slug)
	{
		$data['school'] = School::where('slug',$slug)->first();

		return View::make('school.view',$data);
	}

	public function apply($slug)
	{
		$data['school'] = School::where('slug',$slug)->with('application_fields')->first();

		foreach($data['school']->application_fields as $field){
			$data['tabContent'][$field->section->sortable_id][]	=	$field;
		}

		return View::make('school.apply',$data);
	}

	public function applyProcess($slug)
	{
		$school = School::where('slug',$slug)->with('application_fields')->first();

		$application	=	new Application;
		$application->user_id	=	Auth::user()->id;
		$application->school_id	=	$school->id;
		$application->save();

		$values = Input::all();

		foreach($values as $option => $value){
			$applicationValue	=	new ApplicationValue;
			$applicationValue->option	=	$option;
			$applicationValue->value	=	$value;
			$applicationValue->application_id	=	$application->id;
			$applicationValue->save();
		}

		return Redirect::back()->withSuccess('Form submitted successfully');
	}


}